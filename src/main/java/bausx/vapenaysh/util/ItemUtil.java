package bausx.vapenaysh.util;

import org.spongepowered.api.data.manipulator.mutable.DisplayNameData;
import org.spongepowered.api.data.manipulator.mutable.DyeableData;
import org.spongepowered.api.item.inventory.ItemStack;

/**
 * Created by Jodic on 4/3/2016.
 */
public class ItemUtil {

    public static String getCompositeKey(ItemStack itemStack) {
        ItemStack item = itemStack.copy();
        String string = "";
        string += item.getItem().getName();

        if (item.supports(DisplayNameData.class)) {
            String disName = item.getOrCreate(DisplayNameData.class).get().displayName().get().toPlain();
            string += disName;
        }
        try {
            string += item.getOrCreate(DyeableData.class).get().type().get().getName();
        } catch (Exception ex) {}
        return string;
    }
}
