package bausx.vapenaysh.util;

import bausx.vapenaysh.Main;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.scheduler.Task;

import java.util.concurrent.TimeUnit;

/**
 * Created by Jodic on 4/5/2016.
 */
public class InventoryUtil {
    public static void removeItemFromInventory(Inventory inventory, ItemStack itemStack, int num) {

            Task.builder().execute(() -> {
            int total = num;
            Iterable<Slot> slots = inventory.slots();
            for (Slot slot : slots) {
                if (slot.contains(itemStack)) {
                    int amountPresent = slot.getStackSize();
                    if (total >= amountPresent) {
                        slot.clear();
                        total-=amountPresent;
                    }
                    else {
                        slot.clear();
                        addItemToInvNoWait(slot, itemStack, amountPresent-total);
                        return;
                    }
                }
            }
        }).delay(50, TimeUnit.MILLISECONDS).submit(Main.getPlugin());
    }

    private static void addItemToInvNoWait(Inventory inventory, ItemStack itemStack, int num) {
        int total = num;
        while (total > itemStack.getMaxStackQuantity()) {
            ItemStack add = itemStack.copy();
            add.setQuantity(itemStack.getMaxStackQuantity());
            total = total - itemStack.getMaxStackQuantity();
            inventory.offer(add);
        }
        if (total != 0) {
            ItemStack add = itemStack.copy();
            add.setQuantity(total);
            inventory.offer(add);
        }
    }


    public static void addItemToInventory(Inventory inventory, ItemStack itemStack, int num) {
        Task.builder().execute(() -> {
            addItemToInvNoWait(inventory, itemStack, num);
        }).delay(50, TimeUnit.MILLISECONDS).submit(Main.getPlugin());
    }
}
