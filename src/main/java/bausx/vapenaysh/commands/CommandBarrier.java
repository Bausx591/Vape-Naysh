package bausx.vapenaysh.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/13/2016.
 */
public class CommandBarrier implements CommandCallable{
    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(source instanceof Player){
            List<PotionEffect> effects = new ArrayList<>();
            effects.add(PotionEffect.builder().potionType(PotionEffectTypes.ABSORPTION).amplifier(0).duration(50).particles(true).build());
            Player player = (Player) source;
            player.offer(Keys.POTION_EFFECTS,effects);
        } else {
            source.sendMessage(Text.of(TextColors.DARK_RED, "You are not a player"));
        }
        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.barrier");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of("Gives you a life-saving, outplaying barrier for those clench moments"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of("Gives you a life-saving, outplaying barrier for those clench moments"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
