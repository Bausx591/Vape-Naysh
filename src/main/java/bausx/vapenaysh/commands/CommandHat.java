package bausx.vapenaysh.commands;

import bausx.vapenaysh.util.InventoryUtil;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/9/2016.
 */
public class CommandHat implements CommandCallable{
    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(source instanceof Player){
            Player player = (Player) source;
            Optional<ItemStack> itemInHand = player.getItemInHand();
            if(!itemInHand.isPresent()){
                player.sendMessage(Text.of(TextColors.DARK_RED, "No item in hand"));
            }
            ItemStack oldHelmet = player.getHelmet().isPresent() ? player.getHelmet().get() : null;
            ItemStack newHelmet = itemInHand.get();
            if(newHelmet.getQuantity() > 1){
                newHelmet.setQuantity(newHelmet.getQuantity()-1);
                player.setItemInHand(newHelmet);
            } else {
                player.setItemInHand(null);
            }
            newHelmet.setQuantity(1);
            player.setHelmet(newHelmet);
            if(oldHelmet != null){
                InventoryUtil.addItemToInventory(player.getInventory(), oldHelmet, oldHelmet.getQuantity());
            }
        }
        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.hat");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Puts the item in your hand, on your head"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Puts the item in your hand, on your head"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
