package bausx.vapenaysh.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/9/2016.
 */
public class CommandVanish implements CommandCallable {
    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(source instanceof Player){
            Player player = (Player) source;
            if(player.get(Keys.INVISIBLE).isPresent() && !player.get(Keys.INVISIBLE).get()){
                player.offer(Keys.INVISIBLE, true);
                player.sendMessage(Text.of(TextColors.DARK_PURPLE,"You are now as visible as NA at worlds"));
            } else {
                player.offer(Keys.INVISIBLE, false);
                player.sendMessage(Text.of(TextColors.RED, "You are SKT TI Faker"));
            }
        }
        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.vanish");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Makes you as visible as NA at worlds"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Makes you as visible as NA at worlds"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
