package bausx.vapenaysh.commands;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.enums.JuiceType;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.*;

/**
 * Created by Jodic on 4/3/2016.
 */
public class CommandGiveVapeJuice implements CommandCallable {
    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Gives Vape Juice"));
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Gives Vape Juice"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }

    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(!(source instanceof Player)){
            source.sendMessage(Text.of(TextColors.AQUA, "Only Players can use this command"));
            return CommandResult.success();
        }
        List<String> args = new ArrayList<>(Arrays.asList(arguments.split(" ")));
        Player player = (Player) source;
        ItemStack item = null;

        if(args.size() == 0){
            source.sendMessage(Text.of(TextColors.DARK_RED,"Put and item"));
            return CommandResult.success();
        }

        try{
            item = Main.getPlugin().getJuiceRegistry().getJuiceFromRegistry(JuiceType.valueOf(args.get(0).toUpperCase())).get().getSnapshot().createStack();
        } catch(Exception e) {
            player.sendMessage(Text.of(TextColors.DARK_RED,"Not a valid juice name"));
            return CommandResult.success();
        }

        player.getInventory().offer(item);

        return  CommandResult.success();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vj.givepen");
    }
}
