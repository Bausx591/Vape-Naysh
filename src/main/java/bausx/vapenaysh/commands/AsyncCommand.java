package bausx.vapenaysh.commands;

import bausx.vapenaysh.Main;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;

/**
 * Created by Jodic on 4/10/2016.
 */
public abstract class AsyncCommand implements CommandCallable{

    public abstract void executeAsync(CommandSource src, String args);


    @Override
    public CommandResult process(CommandSource src, String args) throws CommandException {
        Sponge.getScheduler().createAsyncExecutor(Main.getPlugin()).execute(() -> executeAsync(src, args));
        return CommandResult.success();
    }

}
