package bausx.vapenaysh.commands;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.enums.VapeCigType;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/3/2016.
 */
public class CommandGiveVapePen implements CommandCallable {
    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Gives Vape Pen"));
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Gives Vape Pen"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }

    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(!(source instanceof Player)){
            source.sendMessage(Text.of(TextColors.AQUA, "Only Players can use this command"));
            return CommandResult.success();
        }

        Player player = (Player) source;

        ItemStack itemStack = Main.getPlugin().getCigRegistry().getVapeFromRegistry(VapeCigType.PEN).get().getSnapshot().createStack();
        player.getInventory().offer(itemStack);
        return  CommandResult.success();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vb.givepen");
    }
}
