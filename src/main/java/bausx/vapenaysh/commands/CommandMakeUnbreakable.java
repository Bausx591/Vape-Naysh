package bausx.vapenaysh.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/3/2016. *In progress*
 */
public class CommandMakeUnbreakable implements CommandCallable {

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Sets your home"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.AQUA, "Sets your home"));
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(!(source instanceof Player)){
            source.sendMessage(Text.of(TextColors.DARK_RED, "Only players can use this command"));
            return CommandResult.success();
        }

        Player player = (Player) source;

        List<Text> list = new ArrayList<>();
        list.add(Text.of(TextColors.BLUE, "Forged in the afro of Bob Ross"));
        Optional<ItemStack> item = player.getItemInHand();
        if(!item.isPresent()){
            player.sendMessage(Text.of(TextColors.DARK_RED, "No item in hand"));
        }
        ItemStack itemStack = item.get();
        itemStack.offer(Keys.UNBREAKABLE, true);
        itemStack.offer(Keys.ITEM_LORE, list);
        player.setItemInHand(itemStack);
        return  CommandResult.success();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.unbreakable");
    }
}
