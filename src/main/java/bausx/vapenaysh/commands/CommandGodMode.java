package bausx.vapenaysh.commands;

import bausx.vapenaysh.Main;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/10/2016.
 */
public class CommandGodMode extends AsyncCommand {
    @Override
    public void executeAsync(CommandSource source, String arguments) {
        if(source instanceof Player){
            Player player = (Player) source;
            if(Main.godPlayers.contains(player.getUniqueId())) {
                Main.godPlayers.remove(player.getUniqueId());
                player.sendMessage(Text.of(TextColors.RED, "You can bleed again"));
            }
            else {
                Main.godPlayers.add(player.getUniqueId());
                player.sendMessage(Text.of(TextColors.GOLD, "You are a legend"));
            }
        } else {
            source.sendMessage(Text.of(TextColors.DARK_RED, "You are not a player"));
        }
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.god");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Makes you a god"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of(TextColors.GREEN, "Makes you a god"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
