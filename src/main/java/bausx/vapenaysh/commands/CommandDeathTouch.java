package bausx.vapenaysh.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.meta.ItemEnchantment;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.Enchantments;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/10/2016.
 */
public class CommandDeathTouch implements CommandCallable{

    private List<Text> list = new ArrayList<>();


    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if(source instanceof Player){
            Player player = (Player) source;
            Optional<ItemStack> itemInHand = player.getItemInHand();
            if(itemInHand.isPresent()){
                 List<ItemEnchantment> enchantments = new ArrayList<>();
                enchantments.add(new ItemEnchantment(Enchantments.SHARPNESS, 999));
                list.add(Text.of(TextColors.DARK_RED, "Anything touched with this blade will die"));
                itemInHand.get().offer(Keys.ITEM_ENCHANTMENTS, enchantments);
                itemInHand.get().offer(Keys.ITEM_LORE, list);
                player.setItemInHand(itemInHand.get());
            }

        }
        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.deathtouch");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of("Gives your item the touch of death"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of("Gives your item the touch of death"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
