package bausx.vapenaysh.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.Text;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jodic on 4/13/2016.
 */
public class CommandIgnite implements CommandCallable {
    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {

        //if(){

            //.offer(Keys.FIRE_TICKS, 100);
       // } else {
      //      source.sendMessage(Text.of(TextColors.DARK_RED, "You are not a player"));
       // }

        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments) throws CommandException {
        return Collections.emptyList();
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return source.hasPermission("vn.ignite");
    }

    @Override
    public Optional<? extends Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of("Blazes a player into flames"));
    }

    @Override
    public Optional<? extends Text> getHelp(CommandSource source) {
        return Optional.of(Text.of("Blazes a player into flames"));
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.of();
    }
}
