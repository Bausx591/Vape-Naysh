package bausx.vapenaysh.listeners;

import bausx.vapenaysh.Main;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.DamageEntityEvent;

/**
 * Created by Jodic on 4/10/2016.
 */


public class DamageListener {

        @Listener
        public void onPlayerDamage(DamageEntityEvent event){
            if(event.getTargetEntity() instanceof Player){
                Player player = (Player) event.getTargetEntity();

                if(Main.godPlayers.contains(player.getUniqueId())){
                    event.setBaseDamage(0);
                    event.setCancelled(true);
                }
            }
        }
    }

