package bausx.vapenaysh.listeners;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.vapecigs.IVapeCigs;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

/**
 * Created by Jodic on 4/5/2016.
 */

public class VapeListener {

    @Listener
    public void onVape(InteractBlockEvent.Secondary event){
        if(!event.getCause().first(Player.class).isPresent()){
            return;
        }

        Player player = event.getCause().first(Player.class).get();

        Optional<ItemStack> item = player.getItemInHand();

        if(!item.isPresent()){
            return;
        }

        Optional<IVapeCigs> vapeop = Main.getPlugin().getCigRegistry().getVapeFromRegistry(item.get());

        if(!vapeop.isPresent()){
            return;
        }

        player.sendMessage(Text.of(TextColors.DARK_GREEN, "Stay Calm, Vape On. -Vape Nation 2016"));

        vapeop.get().onClick(player);

    }
}
