package bausx.vapenaysh.listeners;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.vapejuices.IVapeJuice;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.UseItemStackEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

/**
 * Created by Jodic on 4/3/2016.
 */
public class JuiceListener {
    @Listener
    public void onUse(UseItemStackEvent.Finish event){
        if(!event.getCause().first(Player.class).isPresent()){
            return;
        }

        Player player = event.getCause().first(Player.class).get();

        ItemStack item = event.getItemStackInUse().getOriginal().createStack();

        Optional<IVapeJuice> juiceop = Main.getPlugin().getJuiceRegistry().getJuiceFromRegistry(item);
        if(!juiceop.isPresent()){
            return;
        }
        player.sendMessage(Text.of(TextColors.DARK_PURPLE, "Straight from the bottle? Damn Daniel."));
        juiceop.get().onClick(player);

    }
}
