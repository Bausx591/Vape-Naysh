package bausx.vapenaysh.listeners;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

import static org.spongepowered.api.text.Text.builder;

/**
 * Created by Jodic on 4/3/2016.
 */
public class LoginListener {
    @Listener
    public void onLogin(ClientConnectionEvent.Join event){
        Player player = event.getTargetEntity();
        Text text = Text.of(TextColors.DARK_GREEN, "Welcome to the Vape Naysh Yall, Go Green!");
        Text clickable = Text.of(TextColors.GREEN, builder("Click here to learn more").onClick(TextActions.runCommand("Our mission here at Vape Nation is to rip the fattest vape, and make the fattest clouds.")));
        player.sendMessage(text);
        player.sendMessage(clickable);

    }
}