package bausx.vapenaysh.vapecigs;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.enums.JuiceType;
import bausx.vapenaysh.enums.VapeCigType;
import bausx.vapenaysh.vapejuices.IVapeJuice;
import bausx.vapenaysh.vapejuices.JuiceSaiyan;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jodic on 4/5/2016.
 */
public class VapeBox implements IVapeCigs {

    private ParticleEffect particle;

    private ItemStackSnapshot snapshot;

    private Map<Player, Task> taskMap = new HashMap<>();

    public VapeBox(){
        ItemStack itemStack = ItemStack.builder().itemType(ItemTypes.GOLDEN_CARROT).build();
        itemStack.offer(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_BLUE, "Vape Box"));
        snapshot = itemStack.createSnapshot();
        particle = ParticleEffect.builder().type(ParticleTypes.CLOUD).count(400).offset(new Vector3d(1.5, 1.5, 1.5)).build();
    }

    @Override
    public ItemStackSnapshot getSnapshot() {
        return snapshot;
    }

    @Override
    public VapeCigType getType() {
        return VapeCigType.BOX;
    }

    @Override
    public void onClick(Player player) {
        IVapeJuice vapeJuice = null;

        Iterable<Slot> slots = player.getInventory().slots();
        for(Slot s : slots){
            Optional<ItemStack> item = s.peek();
            if(!item.isPresent()){
                continue;
            }

            ItemStack itemStack = item.get();

            Optional<IVapeJuice> juice = Main.getPlugin().getJuiceRegistry().getJuiceFromRegistry(itemStack);
            if(!juice.isPresent()){
                continue;
            }

            vapeJuice = juice.get();
            s.clear();
            break;

        }

        if(vapeJuice == null){
            player.sendMessage(Text.of(TextColors.DARK_RED, "Out of juice! NOTLIKETHIS"));
            return;
        }

        List<PotionEffect> list = new ArrayList<>();
        vapeJuice.getEffects().forEach(potionEffect -> list.add(PotionEffect.builder().ambience(true).duration(potionEffect.getDuration()).potionType(potionEffect.getType()).amplifier(potionEffect.getAmplifier()*3).build()));
        if(vapeJuice.getType().equals(JuiceType.SAIYAN)){
            ((JuiceSaiyan) vapeJuice).ascend(player);
            player.sendMessage(Text.of(TextColors.DARK_AQUA,"*Deeply Inhales* and this... IS TO GO EVEN FURTHER BEYOND! AAAAHHHHHHHH"));
        } else if(vapeJuice.getType().equals(JuiceType.SALT)){
            player.sendMessage(Text.of(TextColors.DARK_AQUA,"*Deeply Inhales* GOD FUCKING DAMMIT! I HIT THAT GUY 9 TIMES!"));
        }else  if(vapeJuice.getType().equals(JuiceType.MANGO)){
            player.sendMessage(Text.of(TextColors.DARK_AQUA,"*Deeply Inhales* Good Shit, Fruity"));
        }

        player.offer(Keys.POTION_EFFECTS, list);
        PhatClouds(player);
    }

    @Override
    public void PhatClouds(Player player) {
        World world = player.getWorld();

       TimeCounter counter = new TimeCounter();


        taskMap.put(player, Task.builder().reset().execute(()-> {
            counter.increment();
            if(counter.getTime() >= 100){
                taskMap.get(player).cancel();
                taskMap.remove(player);
                return;
            }
            world.spawnParticles(particle,player.getLocation().getPosition());

        }).delay(1, TimeUnit.SECONDS).interval(200,TimeUnit.MILLISECONDS).submit(Main.getPlugin()));

    }

    private class TimeCounter {
        private int time;

        public TimeCounter() {
            time = 0;
        }

        public int getTime() {
            return time;
        }

        public void reset() {
            time = 0;
        }

        public void increment() {
            time++;
        }
    }
}

