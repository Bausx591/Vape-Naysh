package bausx.vapenaysh.vapecigs;

import bausx.vapenaysh.enums.VapeCigType;
import bausx.vapenaysh.util.ItemUtil;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Jodic on 4/5/2016.
 */
public class CigRegistry {
    private Map<String,IVapeCigs> registry = new HashMap<>();

    public void init(){
        register(new VapeBox());
        register(new VapePen());
    }

    private void register(IVapeCigs cig){
        registry.put(ItemUtil.getCompositeKey(cig.getSnapshot().createStack()), cig);
    }

    public Optional<IVapeCigs> getVapeFromRegistry(ItemStack item){
        String key = ItemUtil.getCompositeKey(item);
        if(registry.containsKey(key)){
            return Optional.of(registry.get(key));
        }
        return Optional.empty();
    }

    public Optional<IVapeCigs> getVapeFromRegistry(VapeCigType type){
        for(IVapeCigs vape : registry.values()){
            if(vape.getType().equals(type)){
                return Optional.of(vape);
            }
        }
        return Optional.empty();
    }
}
