package bausx.vapenaysh.vapecigs;

import bausx.vapenaysh.enums.VapeCigType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;

/**
 * Created by Jodic on 4/5/2016.
 */
public interface IVapeCigs {

     ItemStackSnapshot getSnapshot();

     void onClick(Player player);

     VapeCigType getType();

     void PhatClouds(Player player);

}
