package bausx.vapenaysh.vapejuices;

import bausx.vapenaysh.enums.JuiceType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jodic on 4/3/2016.
 */
public class JuiceHeal implements IVapeJuice {
    private List<PotionEffect> list = new ArrayList<>();
    private ItemStackSnapshot snapshot;

    public JuiceHeal(){
        ItemStack item = ItemStack.builder().itemType(ItemTypes.POTION).build();
        item.offer(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "Heal"));
        snapshot = item.createSnapshot();
        list.add(PotionEffect.builder().potionType(PotionEffectTypes.REGENERATION).amplifier(5).duration(420).particles(true).build());
        list.add(PotionEffect.builder().potionType(PotionEffectTypes.ABSORPTION).amplifier(5).duration(420).particles(true).build());
    }

    @Override
    public ItemStackSnapshot getSnapshot() {
        return snapshot;
    }



    @Override
    public void onClick(Player player) {
        player.offer(Keys.POTION_EFFECTS,list);
        player.sendMessage(Text.of(TextColors.GOLD, "*Deeply Inhales* Not even close!"));
    }

    @Override
    public JuiceType getType() {
        return JuiceType.HEAL;
    }

    @Override
    public List<PotionEffect> getEffects() {
        return list;
    }
}
