package bausx.vapenaysh.vapejuices;

import bausx.vapenaysh.enums.JuiceType;
import bausx.vapenaysh.util.ItemUtil;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.*;

/**
 * Created by Jodic on 4/3/2016.
 */
public class JuiceRegistry {

    private Map<String,IVapeJuice> registry = new HashMap<>();

    public void init(){

        register(new JuiceMango());
        register(new JuiceSalt());
        register(new JuiceSaiyan());
        register(new JuiceHeal());
    }

    private void register(IVapeJuice juice){
        registry.put(ItemUtil.getCompositeKey(juice.getSnapshot().createStack()), juice);
    }

    public Optional<IVapeJuice> getJuiceFromRegistry(ItemStack item){
        String key = ItemUtil.getCompositeKey(item);
        if(registry.containsKey(key)){
            return Optional.of(registry.get(key));
        }
        return Optional.empty();
    }

    public Optional<IVapeJuice> getJuiceFromRegistry(JuiceType type){
        for(IVapeJuice vape : registry.values()){
            if(vape.getType().equals(type)){
                return Optional.of(vape);
            }
        }
        return Optional.empty();
    }
}
