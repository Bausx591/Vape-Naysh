package bausx.vapenaysh.vapejuices;

import bausx.vapenaysh.enums.JuiceType;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;

import java.util.List;

/**
 * Created by Jodic on 4/3/2016.
 */
public interface IVapeJuice {

    ItemStackSnapshot getSnapshot();

    void onClick(Player player);

    JuiceType getType();

    List<PotionEffect> getEffects();

}
