package bausx.vapenaysh.vapejuices;

import bausx.vapenaysh.Main;
import bausx.vapenaysh.enums.JuiceType;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jodic on 4/3/2016.
 */
public class JuiceSaiyan implements IVapeJuice {
    private List<PotionEffect> list = new ArrayList<>();

    private ItemStackSnapshot snapshot;

    private ParticleEffect particle;

    private Map<Player, Task> taskMap = new HashMap<>();

    public JuiceSaiyan(){
        ItemStack item = ItemStack.builder().itemType(ItemTypes.POTION).build();
        item.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Saiyan"));
        snapshot = item.createSnapshot();
        list.add(PotionEffect.builder().potionType(PotionEffectTypes.STRENGTH).amplifier(3).duration(420).particles(true).build());
        list.add(PotionEffect.builder().potionType(PotionEffectTypes.SPEED).amplifier(1).duration(420).particles(true).build());
        list.add(PotionEffect.builder().potionType(PotionEffectTypes.HASTE).amplifier(2).duration(420).particles(true).build());
        particle = ParticleEffect.builder().type(ParticleTypes.VILLAGER_ANGRY).count(35).offset(new Vector3d(1.5, 1.5, 1.5)).build();
    }

    public void ascend(Player player){
        player.offer(Keys.CAN_FLY, true);
        player.offer(Keys.FLYING_SPEED,.2);

        World world = player.getWorld();
        TimeCounter counter = new TimeCounter();

        taskMap.put(player,Task.builder().reset().execute(()-> {
            counter.increment();
            if(counter.getTime() >= 100){
                taskMap.get(player).cancel();
                taskMap.remove(player);
                return;
            }
            world.spawnParticles(particle,player.getLocation().getPosition());

        }).delay(1,TimeUnit.SECONDS).interval(200,TimeUnit.MILLISECONDS).submit(Main.getPlugin()));

        Task.builder().reset().execute(()-> {
            player.offer(Keys.IS_FLYING, false);
            player.offer(Keys.CAN_FLY, false);
            player.offer(Keys.FLYING_SPEED,.1);
        }).delay(20, TimeUnit.SECONDS).submit(Main.getPlugin());
    }

    @Override
    public ItemStackSnapshot getSnapshot() {
        return snapshot;
    }



    @Override
    public void onClick(Player player) {
        player.offer(Keys.POTION_EFFECTS,list);
        ascend(player);
        player.sendMessage(Text.of(TextColors.DARK_AQUA,"*Deeply Inhales* and this... IS TO GO EVEN FURTHER BEYOND! AAAAHHHHHHHH"));
    }

    @Override
    public List<PotionEffect> getEffects() {
        return list;
    }

    @Override
    public JuiceType getType() {
        return JuiceType.SAIYAN;
    }

    private class TimeCounter {
        private int time;

        public TimeCounter() {
            time = 0;
        }

        public int getTime() {
            return time;
        }

        public void reset() {
            time = 0;
        }

        public void increment() {
            time++;
        }
    }
}

