package bausx.vapenaysh.vapejuices;

import bausx.vapenaysh.enums.JuiceType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jodic on 4/3/2016.
 */
public class JuiceMango implements IVapeJuice {
    private List<PotionEffect> list = new ArrayList<>();

    private ItemStackSnapshot snapshot;

    public JuiceMango(){
        ItemStack item = ItemStack.builder().itemType(ItemTypes.POTION).build();
        item.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Mango"));
        snapshot = item.createSnapshot();
        PotionEffect effect = PotionEffect.builder().potionType(PotionEffectTypes.JUMP_BOOST).amplifier(5).duration(420).particles(true).build();
        list.add(effect);
    }

    @Override
    public ItemStackSnapshot getSnapshot() {
        return snapshot;
    }



    @Override
    public void onClick(Player player) {
        player.offer(Keys.POTION_EFFECTS,list);
        player.sendMessage(Text.of(TextColors.GOLD, "*Deeply Inhales* Good Stuff, Fruity."));
    }

    @Override
    public JuiceType getType() {
        return JuiceType.MANGO;
    }

    @Override
    public List<PotionEffect> getEffects() {
        return list;
    }
}
