package bausx.vapenaysh.config;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

import java.util.UUID;

/**
 * Created by Jodic on 4/4/2016.
 */
public class WrappedLocationSerializer implements TypeSerializer<WrappedLocation> {
    @Override
    public WrappedLocation deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
        if (!(value.getParent() == null)) {
            UUID worldUUID = value.getNode("WorldUUID").getValue(new TypeToken<UUID>() {
            });
            int x = value.getNode("X").getInt();
            int y = value.getNode("Y").getInt();
            int z = value.getNode("Z").getInt();
            return new WrappedLocation(x, y, z,worldUUID);
        }
        //TODO Complete shit refactor later
        String s = value.getString();
        String[] args = s.split(",");
        UUID worldUUID = null;
        int x = 0, y = 0, z = 0;

        for (int i = 0; i < 4; i++) {
            String[] values = args[i].split("=");
            String stringValue = values[1].replace("}", "");
            if (i == 0) {
                worldUUID = UUID.fromString(stringValue);
            } else if (i == 1) {
                x = Integer.parseInt(stringValue);
            } else if (i == 2) {
                y = Integer.parseInt(stringValue);
            } else if (i == 3) {
                z = Integer.parseInt(stringValue);
            }
        }
        return new WrappedLocation(x, y, z,worldUUID);
    }

    @Override
    public void serialize(TypeToken<?> type, WrappedLocation obj, ConfigurationNode value) throws ObjectMappingException {
        value.getNode("WorldUUID").setValue(new TypeToken<UUID>() {}, obj.getWorldUUID());
        value.getNode("X").setValue(obj.getX());
        value.getNode("Y").setValue(obj.getY());
        value.getNode("Z").setValue(obj.getZ());
    }
}
