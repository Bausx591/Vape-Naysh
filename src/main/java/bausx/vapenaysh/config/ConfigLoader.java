package bausx.vapenaysh.config;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Jodic on 4/4/2016.
 */
public class ConfigLoader {

    private File folder = new File("config" + File.separator + "Vape-Naysh");
    private File file = new File(folder.getPath() + File.separator + "PlayerHomes.conf");
    private Map<UUID, WrappedLocation> homeRegistry = new HashMap<>();

    public void loadConfiguration(){
        if (!folder.exists()) {
           folder.mkdirs();
        }
        if(!file.exists()){
            try{
                file.createNewFile();
            } catch(IOException e) {
                e.printStackTrace();
            }
            return;
        }

        ConfigurationLoader<CommentedConfigurationNode> cfgManager = HoconConfigurationLoader.builder().setFile(file).build();
        CommentedConfigurationNode config;
        try{
            config = cfgManager.load();
            homeRegistry = config.getNode("Registry").getValue(new TypeToken<Map<UUID, WrappedLocation>>() {}, homeRegistry);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void saveConfiguration(){
        ConfigurationLoader<CommentedConfigurationNode> cfgManager = HoconConfigurationLoader.builder().setFile(file).build();
        CommentedConfigurationNode config;
        try{
            config = cfgManager.load();
            config.getNode("Registry").setValue(new TypeToken<Map<UUID, WrappedLocation>>() {}, homeRegistry);
            cfgManager.save(config);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Optional<Location<World>> getPlayerHome(UUID uuid){
        if(!homeRegistry.containsKey(uuid)){
            return Optional.empty();
        }

        return homeRegistry.get(uuid).toLocation();
    }

    public void setPlayerHome(UUID uuid, Location<World> location){
        homeRegistry.put(uuid, new WrappedLocation(location));
    }


}
