package bausx.vapenaysh.config;

import bausx.vapenaysh.Main;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by Jodic on 4/4/2016.
 */
public class WrappedLocation {
    private int x;
    private int y;
    private int z;
    private UUID worldUUID;

    public WrappedLocation(int x, int y, int z, UUID worldUUID){
        this.x = x;
        this.y = y;
        this.z = z;
        this.worldUUID = worldUUID;
    }

    public WrappedLocation(int x, int y, int z, String worldUUID){
        this(x,y,z,UUID.fromString(worldUUID));
    }

    public WrappedLocation(Location<World> location){
        this(location.getBlockX(),location.getBlockY(),location.getBlockZ(), location.getExtent().getUniqueId());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public UUID getWorldUUID() {
        return worldUUID;
    }

    public Optional<Location<World>> toLocation() {
        return Optional.of(new Location<>(Main.getPlugin().getGame().getServer().getWorld(getWorldUUID()).get(), getX(), getY(), getZ()));
    }


}

