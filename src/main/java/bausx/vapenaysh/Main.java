package bausx.vapenaysh;

import bausx.vapenaysh.commands.*;
import bausx.vapenaysh.config.ConfigLoader;
import bausx.vapenaysh.config.WrappedLocation;
import bausx.vapenaysh.config.WrappedLocationSerializer;
import bausx.vapenaysh.listeners.DamageListener;
import bausx.vapenaysh.listeners.JuiceListener;
import bausx.vapenaysh.listeners.LoginListener;
import bausx.vapenaysh.listeners.VapeListener;
import bausx.vapenaysh.vapecigs.CigRegistry;
import bausx.vapenaysh.vapejuices.JuiceRegistry;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializers;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Jodic on 4/2/2016.
 */
@Plugin(id = "makeamericagreatagain", name = "Make America Great Again", version = "0.0.1")
public class Main {

    private static Main plugin;
    private Logger logger;
    private Game game;
    private CigRegistry cigRegistry;
    private JuiceRegistry juiceRegistry;
    private ConfigLoader loader;
    public static Set<UUID> godPlayers = new HashSet<>();


    @Listener
    public void preinit(GamePreInitializationEvent event){
        plugin = this;
    }

    @Listener
    public void init(GameInitializationEvent event) {
        registerListeners();
        TypeSerializers.getDefaultSerializers().registerType(TypeToken.of(WrappedLocation.class), new WrappedLocationSerializer());
    }

    @Listener
    public void onSeverStart(GameStartingServerEvent event){
        juiceRegistry = new JuiceRegistry();
        juiceRegistry.init();
        cigRegistry = new CigRegistry();
        cigRegistry.init();
        registerCommands();
        loader = new ConfigLoader();
        loader.loadConfiguration();
    }

    @Listener
    public void onServerStop(GameStoppingServerEvent event){
        loader.saveConfiguration();
    }

    private void registerCommands(){
        getGame().getCommandManager().register(getPlugin(), new CommandGiveVapeJuice(), "vapejuice", "j");
        getGame().getCommandManager().register(getPlugin(), new CommandGiveVapeBox(), "vapebox", "b");
        getGame().getCommandManager().register(getPlugin(), new CommandSetHome(), "vapesethome");
        getGame().getCommandManager().register(getPlugin(), new CommandHome(), "vapehome");
        getGame().getCommandManager().register(getPlugin(), new CommandMakeUnbreakable(), "Unbreakable", "ub");
        getGame().getCommandManager().register(getPlugin(), new CommandGiveVapePen(), "vapepen", "pen");
        getGame().getCommandManager().register(getPlugin(), new CommandVanish(), "vapevanish", "v");
        getGame().getCommandManager().register(getPlugin(), new CommandHat(), "vapehat");
        getGame().getCommandManager().register(getPlugin(), new CommandGodMode(), "vapegod");
        getGame().getCommandManager().register(getPlugin(), new CommandDeathTouch(), "die");
        getGame().getCommandManager().register(getPlugin(), new CommandBarrier(), "barrier");
        getGame().getCommandManager().register(getPlugin(), new CommandGhost(), "ghost");
    }

    private void registerListeners(){
        getGame().getEventManager().registerListeners(getPlugin(), new LoginListener());
        getGame().getEventManager().registerListeners(getPlugin(), new JuiceListener());
        getGame().getEventManager().registerListeners(getPlugin(), new VapeListener());
        getGame().getEventManager().registerListeners(getPlugin(), new DamageListener());
    }

    @Inject
    private void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Inject
    private void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public Logger getLogger() {
        return logger;
    }

    public static Main getPlugin() {
        return plugin;
    }

    public JuiceRegistry getJuiceRegistry() {
        return juiceRegistry;
    }

    public CigRegistry getCigRegistry() {
        return cigRegistry;
    }

    public ConfigLoader getLoader() {
        return loader;
    }

}
